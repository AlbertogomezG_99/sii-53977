//Mundo Servidor.h

#if !defined(AFX_MUNDOSERVIDOR_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDOSERVIDOR_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "Raqueta.h"
#include "DatosMemCompartida.h"
#include "Puntuaciones.h"
#include "Socket.h"


class CMundoServidor  
{
public:
	void Init();
	CMundoServidor();
	virtual ~CMundoServidor();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();
	void RecibeComandosJugador();	
	
	
	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;
	Puntuacion puntos;
	int puntos1;
	int puntos2;
	int fd; //descriptor de fichero de la FIFO
	DatosMemCompartida DatosMemCompartidos;
	DatosMemCompartida *pDatosMemCompartidos;
	//int fifo_servidor_cliente;
	//int fifo_cliente_servidor;

	pthread_t thid1;
	pthread_attr_t atrib;
	
	Socket Socket_conexion;
	Socket Socket_comunicacion;

	
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
