//servidor
#include "glut.h"
#include "MundoServidor.h"


CMundoServidor mundoServidor;//Cada vez que se cree un objeto 
//de CmundoCliente lo primero que hará es ir a su constructor
//por eso no es necesario poner mundoCliente.Init() mas adelante

//NO HACE FALTA LLAMARLAS EXPLICITAMENTE
void OnDraw(void); 
void OnTimer(int value); 
void OnKeyboardDown(unsigned char key, int x, int y); 
int main(int argc,char* argv[])
{
	
	glutInit(&argc, argv);
	glutInitWindowSize(800,600);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutCreateWindow("mundoServidor");

	glutDisplayFunc(OnDraw);
	//glutMouseFunc(OnRaton);

	glutTimerFunc(25,OnTimer,0);
	glutKeyboardFunc(OnKeyboardDown);
	glutSetCursor(GLUT_CURSOR_FULL_CROSSHAIR);
	
	mundoServidor.InitGL();
	glutMainLoop();	

	return 0;   
}

void OnDraw(void)
{
	mundoServidor.OnDraw();
}
void OnTimer(int value)
{
	mundoServidor.OnTimer(value);
	glutTimerFunc(25,OnTimer,0);
	glutPostRedisplay();
}
void OnKeyboardDown(unsigned char key, int x, int y)
{
	mundoServidor.OnKeyboardDown(key,x,y);
	glutPostRedisplay();
}
