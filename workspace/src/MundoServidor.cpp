//Mundo servidor
#include <fstream>
#include "Mundo.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <iostream>
#include <string>
#include<sys/mman.h>

#include "MundoServidor.h"

#include <pthread.h>
#include <signal.h>



//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundoServidor::CMundoServidor()
{
	Init();
}

CMundoServidor::~CMundoServidor()
{
//cerrar la tuberia en el destructor del mundo
      close(fd);
      unlink("/tmp/FIFO");
      //close (fifo_servidor_cliente);
      //close (fifo_cliente_servidor);
}

void* hilo_comandos(void* d)
{
      CMundoServidor* p=(CMundoServidor*) d;
      p->RecibeComandosJugador();
}

void CMundoServidor::RecibeComandosJugador()
{


     while (1) {
            usleep(10);
            char cad[100];
            //read(fifo_cliente_servidor, cad, sizeof(cad));
            Socket_comunicacion.Receive(cad,sizeof(cad));
            unsigned char key;
            sscanf(cad,"%c",&key);
            if(key=='s')jugador1.velocidad.y=-4;
            if(key=='w')jugador1.velocidad.y=4;
            if(key=='l')jugador2.velocidad.y=-4;
            if(key=='o')jugador2.velocidad.y=4;
	   
      }
}

void CMundoServidor::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoServidor::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoServidor::OnTimer(int value)
{	

        if((puntos1==3)||(puntos2==3))exit(1);
        
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
        
        
	esfera.Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		puntos.jugador1=puntos1;
		puntos.jugador2=puntos2;
		puntos.UltimoGanador=2;
		write(fd,&puntos,sizeof(puntos));
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		puntos.jugador1=puntos1;
		puntos.jugador2=puntos2;
		puntos.UltimoGanador=1;
		write(fd,&puntos,sizeof(puntos));
	}
	
	//Actualizar los campos del atributo DatosMemCompartida
	pDatosMemCompartidos-> esfera=esfera;
	pDatosMemCompartidos-> raqueta1=jugador1;
	
	if(pDatosMemCompartidos->accion==-1){
	OnKeyboardDown('s',0,0);
	}
	
	else if(pDatosMemCompartidos->accion==0){}
	
	else if(pDatosMemCompartidos->accion==1){
	OnKeyboardDown('w',0,0);
	}
	
	char cad[200];
	sprintf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d",
		esfera.centro.x,esfera.centro.y, 
		jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,
		jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, 
		puntos1, puntos2);
	//write(fifo_servidor_cliente,cad,sizeof(cad));
	Socket_comunicacion.Send(cad,sizeof(cad));

	
	

}

void CMundoServidor::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;

	}
}

void CMundoServidor::Init()
{

//Conexion con el socket

   char ip[]="127.0.0.1";
      char nombre[20];
   if(Socket_conexion.InitServer(ip,8000)==-1){
      printf("Error al abrir el servidor\n");
   }/*Al ser InitServer, una funcion bloqueante, establecemos un tiempo
   determinado para que salga de ella en caso de que se produzca un error*/
   
   Socket_comunicacion=Socket_conexion.Accept();
 
    Socket_comunicacion.Receive(nombre,sizeof(nombre));
    
    printf("El jugador %s se ha conectado a la partida", nombre);
//Abrir FIFO en el Init del Mundo
      
      if((fd=open("/tmp/FIFO",O_WRONLY))<0){
      perror("Error al abrir el FIFO");
      exit(1); ;
      }
      
//Crear un fichero del tamaño del atributo DatosMemCompartida(open y write)

//open con creat

int fd_mmap=open("/tmp/DatosMemCompartidos",O_CREAT|O_TRUNC|O_RDWR,0666);
    
    if(fd_mmap<0){
    perror("Error al crear el fichero");
    exit(1);
    }
  //Inicialización de DatosMemCompartidos
  
  DatosMemCompartidos.accion=0;
  DatosMemCompartidos.esfera=esfera;
  DatosMemCompartidos.raqueta1=jugador1;
  
  //write
  
  write(fd_mmap,&DatosMemCompartidos,sizeof(DatosMemCompartidos));
  
  //proyeccion en memoria
   pDatosMemCompartidos=static_cast<DatosMemCompartida*>(mmap(NULL,sizeof(DatosMemCompartidos),PROT_READ|PROT_WRITE,MAP_SHARED,fd_mmap,0));// el mmap devuelve un puntero a void. Como queremos hacer un puntero a DatosMemCompartida, hacemos la conversion mediante static_cast
  
  if(pDatosMemCompartidos==MAP_FAILED){
  perror("Error de proyeccion en memoria");
  close (fd_mmap);
  return ;
  }

//cerramos el descriptor de ficheros
 close(fd_mmap);
 
 // EL SERVIDOR ABRE LA TUBERÍA EN MODO ESCRITURA 
 /*fifo_servidor_cliente=open("/tmp/FIFO_SERVIDOR_CLIENTE",O_WRONLY);
 if(fifo_servidor_cliente<0){
      perror("Error al abrir el FIFO_SERVIDOR_CLIENTE");
      return;
 }*/
 
 //en el init del servidor crear el threat
 
	pthread_create(&thid1, NULL, hilo_comandos, this);
 
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
}
