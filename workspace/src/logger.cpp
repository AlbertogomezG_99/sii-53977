#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <iostream>
#include <stdlib.h>
#include <string>
#include "Puntuaciones.h"

int main(){

Puntuacion puntos;
//crear el fifo
  if(mkfifo("/tmp/FIFO",0600)<0){

  perror("Error al crear FIFO");
  return 1;    

  } 

  int fd=open("/tmp/FIFO",O_RDONLY);
  if(fd<0){
  perror("Error al abrir el FIFO");
  return 1;
  }
  
  while(read(fd,&puntos,sizeof(puntos))==sizeof(puntos)){
  if(puntos.UltimoGanador==1)
     printf("Jugador 1 marca 1 punto, lleva un total de %d puntos.\n",puntos.jugador1);
  else if(puntos.UltimoGanador==2)
     printf("Jugador 2 marca 1 punto, lleva un total de %d puntos.\n",puntos.jugador2); 

 }
  close(fd);
  unlink("/tmp/FIFO");
  return 0; 

}
