#include "DatosMemCompartida.h"
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <iostream>
#include <stdlib.h>
#include <string>
#include <sys/mman.h>
#include <fstream>

int main(){
    
    DatosMemCompartida *pDatosMemCompartidos;
    int fd=open("/tmp/DatosMemCompartidos",O_RDWR|O_CREAT);
    if(fd<0){
    
    perror("Error al abrir el fichero");
    return 1;
    }
     pDatosMemCompartidos=static_cast<DatosMemCompartida*>(mmap(0,sizeof(DatosMemCompartida),PROT_READ|PROT_WRITE,MAP_SHARED,fd,0));
     
     if(pDatosMemCompartidos==MAP_FAILED){
     perror("Error de proyección de memoria");
     close(fd);
     return 1;
     }
     close(fd);
     
     while(1){
     
     if((pDatosMemCompartidos->esfera.centro.y)<(pDatosMemCompartidos->raqueta1.y2)/2){
     
     pDatosMemCompartidos->accion=-1;
     }
     
     else if((pDatosMemCompartidos->esfera.centro.y)==(pDatosMemCompartidos->raqueta1.y2)/2){
     
     pDatosMemCompartidos->accion=0;
     }
     
     else if((pDatosMemCompartidos->esfera.centro.y)>(pDatosMemCompartidos->raqueta1.y2)/2){
     
     pDatosMemCompartidos->accion=1;
     }
     
     usleep(25000);
     }
 munmap(pDatosMemCompartidos,sizeof(DatosMemCompartida)); 
 unlink("/tmp/DatosMemCompartidos");  
 
 return 1;  
}
