INCLUDE_DIRECTORIES("${PROJECT_INCLUDE_DIR}")

ADD_EXECUTABLE(logger logger.cpp ${COMMON_SRCS})
TARGET_LINK_LIBRARIES(logger)



SET(COMMON_SRCS 
	MundoCliente.cpp 
	Esfera.cpp
	Plano.cpp
	Raqueta.cpp
	Vector2D.cpp
	Socket.cpp
	)

#EJECUTABLE CLIENTE
ADD_EXECUTABLE(cliente cliente.cpp ${COMMON_SRCS})
TARGET_LINK_LIBRARIES(cliente glut GL GLU)

#EJECUTABLE BOT
ADD_EXECUTABLE(bot bot.cpp ${COMMON_SRCS})
TARGET_LINK_LIBRARIES(bot glut GL GLU)

SET(COMMON_SRCS 
	MundoServidor.cpp 
	Esfera.cpp
	Plano.cpp
	Raqueta.cpp
	Vector2D.cpp
	Socket.cpp
	)

#EJECUTABLE SERVIDOR
ADD_EXECUTABLE(servidor servidor.cpp ${COMMON_SRCS})
TARGET_LINK_LIBRARIES(servidor pthread glut GL GLU)			

